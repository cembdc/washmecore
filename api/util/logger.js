var fs = require('fs');

var Logger = exports.Logger = {};

let logDirectory = __dirname + '/log';
var logDateFile = new Date().toISOString().split('T')[0].replace('-', '').replace('-', '');

if (fs.existsSync(logDirectory) === false) {
  fs.mkdirSync(logDirectory);
}

var infoStream = fs.createWriteStream(logDirectory + '/' + logDateFile + '-info.txt',{flags: 'a'});
var errorStream = fs.createWriteStream(logDirectory + '/' + logDateFile + '-error.txt',{flags: 'a'});
var debugStream = fs.createWriteStream(logDirectory + '/' + logDateFile + '-debug.txt',{flags: 'a'});

Logger.info = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  infoStream.write(message);
};

Logger.debug = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  debugStream.write(message);
};

Logger.error = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  errorStream.write(message);
};