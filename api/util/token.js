var auth = require("rndm");
var token = require("jsonwebtoken");

var createAuth = function (req, userID, complate) {
    var deviceID = sanitize(req.body.device_id);
    var userIDCleaning = sanitize(userID);
    var deviceType = sanitize(req.body.device_type);
    var pushToken = sanitize(req.body.push_token);
    var appVersion = sanitize(req.body.app_v);


    model_auth.update({ user_id: userIDCleaning }, { $set: { status: false } }, { multi: true }, function (obj) {

        var authKey = auth(36);
        var mauth = model_auth({
            user_id: userIDCleaning,
            device_id: deviceID,
            auth_id: authKey,
            status: true,
            device_type: deviceType,
            push_token: pushToken,
            app_v: appVersion
        });
        mauth.save();
        var t = token.sign({ AI: authKey }, utils.string.CRYPTO);
        complate(t);
    });
}

var tokenVerify = function (tkn, complateBlock, errorBlock) {

    token.verify(tkn, utils.string.CRYPTO, function (err, decoded) {
        if (err) {
            utils.message.filter(404, function (obj) {
                errorBlock(obj);
            })
        } else {
            model_auth.find({ auth_id: decoded.AI, status: true }, function (error, auths) {
                if (auths.length == 1) {
                    //Token bilgisi vardır.
                    complateBlock(auths);
                } else {//Token bilgisi bulunamamaktadır.
                    utils.message.filter(404, function (obj) {
                        errorBlock(obj);
                    })
                }

            });
        }
    });


}