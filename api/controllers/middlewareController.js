'use strict';

var logger = require('../util/logger').Logger;

exports.logRequests = (req, res, next) => {

    let body = req._body ? JSON.stringify(req.body) : " - ";
    let msg = req.ip + " | " + req.method + " | " + req.originalUrl + " | " + body;

    logger.info(msg);
    logger.error("Error Message");

    next();

};