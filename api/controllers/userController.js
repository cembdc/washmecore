'use strict';

require('../models/user');
let mongoose = require('mongoose');
var User = mongoose.model('User');

exports.getAllUsers = function (req, res) {

  User.find({}, function (err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.getUser = function (req, res) {

  User.findById(req.params.userId, function (err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.createUser = function (req, res) {

  var newUser = new User(req.body);
  newUser.save(function (err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.updateUser = function (req, res) {

  User.findOneAndUpdate({ _id: req.params.userId }, req.body, { new: true }, function (err, user) {
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.deleteUser = function (req, res) {

  User.remove({
    _id: req.params.userId
  }, function (err, user) {
    if (err)
      res.send(err);
    res.json({ message: 'User successfully deleted' });
  });
};


exports.testget = function (req, res) {
  res.json({ message: 'User successfully deleted' });
};

exports.testpost = function (req, res) {

  res.json({ message: 'User successfully deleted' });
};