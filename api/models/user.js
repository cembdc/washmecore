'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var UserSchema = new Schema({
    firstName: {
        type: String,
        required: 'Kindly enter the name of the user'
    },
    lastName: {
        type: String,
        required: 'Kindly enter the name of the user'
    },
    email: {
        type: String,
        required: 'Kindly enter the name of the user'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
    status: {
        type: [{
            type: String,
            enum: ['active', 'passive']
        }],
        default: ['passive']
    }
});

module.exports = mongoose.model('User', UserSchema);