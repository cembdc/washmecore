'use strict';

let router = require('express').Router();

// Middleware
let middleware = require('../controllers/middlewareController');
router.use(middleware.logRequests);

// Users
let user = require('../controllers/userController');
router.get('/users', user.getAllUsers);
router.get('/user/:userId', user.getUser);
router.post('/user/createUser', user.createUser);
router.post('/user/updateUser', user.updateUser);
router.post('/user/deleteUser', user.deleteUser);
//router.post('/buggyroute', user.buggyRoute);

router.get('/users/testget', user.testget);
router.post('/users/testpost', user.testpost);

// Error Handling
let errors = require('../controllers/errorController');
router.use(errors.errorHandler);

// Request was not picked up by a route, send 404
router.use(errors.nullRoute);

// Export the router
module.exports = router;
