'use strict';

// API boilerplate
let express = require('express');
let app = express();
let routes = require('./api/routes/route');

// Logging
let bodyParser = require('body-parser');
let morgan = require('morgan');
let fs = require('fs');
let FileStreamRotator = require('file-stream-rotator');
let logDirectory = __dirname + '/log';

// Config
let config = require('config');

// BodyParser allows us to get data out of URLs
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Load up the routes
app.use('/', routes);


// Start the API
app.listen(config.apiPort);
console.log("API running on port " + config.apiPort);

// Export API server for testing
module.exports = app;
